# -*- coding: utf-8 -*-
"""
@file FSM_LED.py

This file serves as an example implementation of a finite-state-machine for 
the usage of the Nucleo board.
The example will implement some code to multitask between the control of the
LED withint the Nucleo board as well as an imaginary LED.

@author: horac
"""

import time
import pyb
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer (2, freq=20000)
t2ch1 = tim2.channel(1,pyb.Timer.PWM, pin=pinA5)

class TaskLED:
    '''
    @brief     A finite state machine to control the LEDs
    @details   This class will define some states to control the LED within 
               the nucleo and the imaginary LED
    '''
    
     # Defines the initial state
    S0_INIT      =  0
    
    # Constant defining the first state
    S1_PrintVAL  =  1
    
    # Constant defining the second state
    S2_PrintVAL  =  2
    
    def __init__(self, interval, LED):
        '''
        @brief   It creates a taskLED object
        @detail  This section will create variables that can be used as a 
                 reference for the taskLED object
        
        '''
        ## The state to which the code will be run on the next iteration task
        self.state = self.S0_INIT
        
        # it defines which LED we are using; 0=virtual LED, and 1=Nucleos' LED
        self.LED = LED
                
        # variable defining the counting for the state
        self.VAL = 0
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time  =  time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief    It runs an iteration of Task LED
        @details  This section provides with the main code so the LED within
                  the nucleo board and the imaginary LED can be turn off and
                  on
        '''
        # Setting the initial time 
        self.curr_time = time.time() 
        
        if (self.curr_time >= self.next_time):
            if (self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f} for LED# '.format(self.curr_time - self.start_time) + str(self.LED))
                t2ch1.pulse_width_percent(self.VAL)
                self.transitionTo(self.S1_PrintVAL)
                print ('LED is off and VAL= ' + str(self.VAL))
                
            elif (self.state == self.S1_PrintVAL):
                print(str(self.runs) + ' State 1 {:0.2f} for LED# '.format(self.curr_time - self.start_time) + str(self.LED))
                if (self.VAL < 100):
                    self.VAL += 25
                    t2ch1.pulse_width_percent(self.VAL)
                    self.transitionTo(self.S2_PrintVAL)
                    print ( 'LED is on and VAL= ' + str(self.VAL))
                else:
                    self.VAL = 0
                    print ( 'LED is on and VAL= ' + str(self.VAL))
                
            elif (self.state == self.S2_PrintVAL):
                print(str(self.runs) + ' State 2 {:0.2f} for LED# '.format(self.curr_time - self.start_time) + str(self.LED))
                if (self.VAL < 100):
                    self.VAL += 25
                    t2ch1.pulse_width_percent(self.VAL)
                    self.transitionTo(self.S1_PrintVAL)
                    print ('LED is off and VAL= ' + str(self.VAL))
                else:
                    self.VAL = 0
                    print ( 'LED is on and VAL= ' + str(self.VAL))
            else:
                pass
            
            
            self.runs += 1
            self.next_time += self.interval # updates the time to the current time
                    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState




        

       
        