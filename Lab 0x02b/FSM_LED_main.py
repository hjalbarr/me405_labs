"""
@file FSM_LED_main.py

@author: horac
"""

from FSM_LED import TaskLED


## Task object
task1 = TaskLED(1,1)  # This task defines the LED on Nucleo
task2 = TaskLED(1,0)  # This task defines the virtual LED

# To run the tasks over and over
for N in range(10000000): 
    task1.run()
    task2.run()