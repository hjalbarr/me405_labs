'''
@file Fibonacci_sequency.py

@mainpage

@section sec_intro Introduction
This page has content to ME 305 Horacio's repository

@section sec_document Documentation
The link for the Fibonnaci_sequence program is located at @ref page_fibonacci_src

@page page_fibonnaci Fibonnaci

@section page_fibonacci_src Source Code Access
You can find the source for the Fibonnaci sequence code in the following link:
https://bitbucket.org/hjalbarr/me405_labs/src/master/Lab%201/Fibonacci_sequency.py



'''