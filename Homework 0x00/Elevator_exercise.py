'''
@file Elevator_exercise.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator
car.

The user has a button to go between floor 1 and floor 2.

There is also a limit switch at either end of the floor as well as buttons 
which indicates the floor to which the elevator should move.
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elavator car.
    @details    This class implements a finite state machine to control the
                operation of an elevator car between floors.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN      = 1
    
    ## Constant defining State 2
    S2_MOVING_UP        = 2
    
    ## Constant defining State 3
    S3_STOPPED_FLOOR_1  = 3
    
    ## Constant defining State 4
    S4_STOPPED_FLOOR_2  = 4
    

    
    def __init__(self, interval, Motor, Second, First, Button_1, Button_2):
        '''
        @brief      Creates a TaskElevator object.
        @details    Creates variables than can be used in other classes
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A button object used to indicate if the elevator is at floor_2
        self.Second   = Second
       
        ## The button object used to indicate if the elevator is at floor_1
        self.First    = First
       
        ## The button object used to go to floor 1
        self.Button_1   = Button_1
      
        ## The button object used to go to floor 2
        self.Button_2   = Button_2
      
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of TaskElevator
        @details    It provides with the code for the elevator to move up,
                    down, or remain stationary at floor_1
        '''
        self.curr_time = time.time()    #updating the current timestamp
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code                
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Downwards()
            
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 1 Code
                if self.First.getSensorState():
                    self.transitionTo(self.S3_STOPPED_FLOOR_1)
                    self.Motor.Downwards()
                else:
                    print ( 'Elevator is transitioning to the first floor')
            
            elif(self.state == self.S2_MOVING_UP):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time)) 
                # Run State 2 Code
                if self.Second.getSensorState():
                    self.transitionTo(self.S4_STOPPED_FLOOR_2)
                    self.Motor.Upwards()
                else:
                    print ( 'Elevator is transitioning to second floor')
            
            elif(self.state == self.S3_STOPPED_FLOOR_1):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.Button_2.getButtonState():
                   self.transitionTo(self.S2_MOVING_UP)
                   self.Motor.Stationary()
                else:
                    self.Motor.Stationary()
                    print ('Elevator is on floor 1')
                    
            elif(self.state == self.S4_STOPPED_FLOOR_2):
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                if self.Button_1.getButtonState():
                   self.transitionTo(self.S1_MOVING_DOWN)
                   self.Motor.Stationary()
            
                else:
                    self.Motor.Stationary()
                    print ('Elevator is in the second floor')

                    
            else:
                print ( 'The input provided is not recognize')
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that can be pushed by the
                imaginary driver to go along the different floors. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the state of the different Buttons.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class Floor:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that is pushed by an imaginary
                sensor when the elevator gets to the final floor destinations.
                As of right now,this class is implemented using "pseudo-hardware".
                That is, we are not working with real hardware IO yet, 
                this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getSensorState(self):
        '''
        @brief      Gets the state of first and second sensors.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])
    

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elavator
                car go up to floor 2, down to floor 1, or remain stationary.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Stationary(self):
        '''
        @brief Keeps the motor stationary
        '''
        print('Motor is stationary')

    def Upwards(self):
        '''
        @brief Moves the motor upwards
        '''
        print('Motor moving upwards')
    
    def Downwards(self):
        '''
        @brief Moves the motor downwards
        '''
        print('Motor moving downwards')
    
    

