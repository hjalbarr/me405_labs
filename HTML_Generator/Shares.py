"""
@file Shares.py

@brief A container for all the inter-task variables

Created on Mon Oct 19 11:55:17 2020

@author: horac
"""

## The command character used in the Encoder and User_Interface class to know the recent position of the encoder
En_Update = 0

## The command character used in the Encoder and User_Interface class to know the delta value
Delta = 0



