"""
@file UI.py

Created on Tue Nov  3 13:23:06 2020

@author: horac
"""

import utime
import pyb
from pyb import UART 

class UI:
    '''
    @brief It defines a class for the User Interface interaction with the nucleo
    
    '''
    # constant defining state 0
    S0_INIT     =  0
    
    # constant defining state 1
    S1_check    =  1
    
    # constant defining state 2
    S2_check    =  2
    
    # constant defining state 3
    S3_check    =  3

    def __init__(self):
        '''
        @brief It sets the initial conditions as well as defining some other important variables
        
        '''
        
        # it initiliazed the baudrate
        self.myuart = UART(3, 9600)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        # It sets a variable for pin A5
        self.PinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        # It initiates the "val" variable
        self.val = 0
        
        # It initialized the timer
        self.Timer = pyb.Timer(3)
        
        # It sets the initial frequency of the timer
        self.InitialTimer = self.Timer.init(freq=1)
       
        # it creates the initial state
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()    
        
        # It sets the interval of the LED interval
        self.DesireInterval = 1
        
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(self.DesireInterval*1e6)                             # In this form, 1 interval = 1 second     
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
        
    def LED (self):
        '''
        @brief It defines an object for the LED intervals
        '''
        
        if self.runs % 2 == 0:
            self.PinA5.low()
            self.Time = ((utime.ticks_diff(self.curr_time,self.start_time))*1e-6)
            print('LED is OFF R:{:} T:{:0.2f}'.format(self.runs, self.Time))
     
        else:
            self.PinA5.high()
            self.Time = ((utime.ticks_diff(self.curr_time,self.start_time))*1e-6)
            print('LED is ON R:{:} T:{:0.2f}'.format(self.runs, self.Time))
         
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
   
    def run(self):
        '''
        @brief It creates an object through which the UI class can be run
        '''
        
    
        if self.state == self.S0_INIT:
            
            if self.myuart.any() != 0:
                self.val = self.myuart.readchar()
                self.transitionTo(self.S1_check)
            else:
                self.transitionTo(self.S2_check)
                    
        elif self.state == self.S1_check:    # Check for the input provided and sets it to the corresponding frequency
                
                if self.val == 1:
                    self.DesireInterval = 1   # it sets the value provided in the serial port to the frequency of the LED
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 1')
                    self.transitionTo(self.S2_check)
          
                elif self.val == 2:
                    self.DesireInterval = 1/2
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 2')
                    self.transitionTo(self.S2_check)
        
                elif self.val == 3:
                    self.DesireInterval = 1/3
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 3')
                    self.transitionTo(self.S2_check)
                
                elif self.val == 4:
                    self.DesireInterval = 1/4
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 4')
                    self.transitionTo(self.S2_check)
     
                elif self.val == 5:
                    self.DesireInterval = 1/5
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 5')
                    self.transitionTo(self.S2_check)
                
                elif self.val == 6:
                    self.DesireInterval = 1/6
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 6')
                    self.transitionTo(self.S2_check)
        
                elif self.val == 7:
                    self.DesireInterval = 1/7
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 7')
                    self.transitionTo(self.S2_check)
      
                elif self.val == 8:
                    self.DesireInterval = 1/8
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 8')
                    self.transitionTo(self.S2_check)
        
                elif self.val == 9:
                    self.DesireInterval = 1/9
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 9')
                    self.transitionTo(self.S2_check)
       
                elif self.val == 16:
                    self.DesireInterval = 1/10
                    self.interval = int(self.DesireInterval*1e6)
                    print('send 10')
                    self.transitionTo(self.S2_check)
                    
                else:
                    print('\r\n')
                    print('Not a recognized input')
                    print('please provide a numerical value')
                    print('of frequency between 1-10.')
                    self.transitionTo(self.S3_check)
                    
        elif self.state == self.S2_check:
            
            if self.myuart.any() != 0:
                self.val = self.myuart.readchar()
                self.transitionTo(self.S1_check)
            else:
                self.curr_time = utime.ticks_us()   # It provides with the current time
                if utime.ticks_diff(self.curr_time, self.next_time) >= 0: 
                    self.runs += 1
                    self.LED()
                    self.next_time = utime.ticks_add(self.next_time, self.interval)  #next time at which it will be run
                    self.transitionTo(self.S2_check)      
                    
        elif self.state == self.S3_check:   #It checks if the value provided is within the frequency values given 
            
            if self.myuart.any() != 0:  
                self.val = self.myuart.readchar()
                self.transitionTo(self.S1_check)
            else:
                self.transitionTo(self.S3_check)
                    
            

## Task object
task1 = UI()   # Will run a task for the the encoder

while True:    #Will change to   "while True:" once we're on hardware
   task1.run()