'''
@file mainpage.py

@mainpage





@section sec_intro Introduction
Welcome to Horacio Jr. Albarran Delgado only repository for ME 305. In this online repository website, it can be found 
all of the laboratory assignments that were performed during the Fall 2020 quarter for the class of ME 305 at the 
California Polytechnic State University (Cal Poly) located at San Luis Obispo, Ca. lectured by 
Professor Refvem and Dr. Espinoza-Wade.

@section sec_Fibonacci_code Fibonacci code (lab 0x01)
The link for the Fibonacci sequence code program is located at @ref page_fibonacci_src

@section sec_Elevator_code Elevator code 
The link for the Elevator program as well as the FSM diagram are located at @ref page_elevator

@section sec_LED_code LED code (lab 0x02b)
The link for the LED Blinking code as well as FSM diagram are located at @ref page_LED

@section sec_Encoder_code Encoder code (lab 0x03)
The following link will redirect you to the FSM for the Encoder and User Interface diagrams as well as the necessary codes @ref page_Encoder

@section sec_UI_code User Interface code (lab 0x04)
The following link will redirect you to the FSM for the User Interface as well as providing with the necessary codes @ref page_UI

@section sec_UIPhone_code User Interface Smart-phone code (lab 0x05)
The following link will redirect you to the FSM for the User Interface with an Smart-phone device as well as providing with the app and the code @ref page_UIPhone

@section sec_DCMotor_code DC Motor code (lab 0x06)
The following link will redirect you to the DC Motor codes @ref page_DCMotor

@section sec_ReferenceTracking_code Reference Tracking code (lab 0x07)
The following link will redirect you to the Reference Tracking codes as well as any other helpful additional information regarding such lab @ref page_ReferenceTracking







@page page_fibonnaci Fibonacci task

@section page_fibonacci_Introduction Introduction
This section provides with the sequential Fibonacci code for any real integer

@section page_fibonacci_src Fibonacci Source Code Access
You can find the source for the Fibonnaci sequence code in the following link:
https://bitbucket.org/hjalbarr/me405_labs/src/master/Lab%201/Fibonacci_sequency.py







@page page_elevator Elevator task

@section page_elevator_Introduction Introduction
The code provided in this section is for an elevator that moves along two floors (floor_1 and floor_2)
due to some inputs provided by buttons within the elevator car and sensors that will indicate when the
elevator gets to the first or second floor, respectively. 

@section page_elevator_FSM FSM for Elevator code
The Finite State Machine (FSM) diagram for the elevator is the following 
@image html image.png width=600px

@section page_elevator_src Elevator Source Code Access
The source for the Elevator code can be found in the following link:
https://bitbucket.org/hjalbarr/me405_labs/src/master/Homework%200x00/







@page page_LED LED Blinking task

@section page_LED_Introduction Introduction
The code provided in this section is for the blinking of two LEDs, one within the Nucleo circuit and
another virtual LED, which function asynchronously. The brightness of the LED within the Nucleo circuit
will increase in a sawtooth wave form function whereas the virtual LED will only print and output whether 
the virtual LED is "on" or "off," respectively.

@section page_LED_FSMLED FSM LED
The Finite State Machine (FSM) diagram for the Blinking LEDs is the following
@image html LED.png width=600px

@section page_LED_src LED Source Code Access
The link source for the LED Blinking code is provided in the following link:
https://bitbucket.org/hjalbarr/me405_labs/src/master/Lab%200x02b/







@page page_Encoder Encoder and User Interface task

@section page_Encoder_Introduction Introduction
The following codes provided in this section are for the usage of an Encoder connected to a Nucleo-L476RG 
in which the Encoder position is continuously been updated and its position is been provides. More so, the 
difference between the temporary "old" and "new" readings from the Encoder, delta, is also obtained and updated.
A User_Interface code is also provided, and it allows for an input from a user while the Encoder code is 
running in the background; The user interface can only happen through an external terminal emulator, such as puTTy

@section page_FSMEncoder FSM Encoder
The Finite State Machine (FSM) diagram for the Encoder is the following
@image html Encoder.PNG width=600px

@section page_FSMUserInterface FSM User Interface
The Finite State Machine (FSM) diagram for the User_Interface is the following
@image html UserInterface.PNG width=600px

@section page_Encoder_src Encoder Source Code Access
The link source for the various codes needed for the Encoder and User_Interface are provided in the following link:
https://bitbucket.org/hjalbarr/me405_labs/src/master/Lab%203/





@page page_UI User Interface and Main code

@section page_UI_Introduction Introduction
The following codes provided in this section are for the usage of an Encoder connected to a Nucleo-L476RG in which 
a user can interact with the code through a Python script in which the Encoder position will continuously be provided 
on the console window and a graph with the collected data will be displayed once the Python script is stopped within 
the console window.

@section page_FSMMain FSM for Main code 
The Finite State Machine (FSM) for the Main code is the following
@image html Python.PNG width=600px

@section page_FSMUI FSM for User Interface
The Finite State Machine (FSM) for the User Interface code is the following
@image html UI.PNG width=600px

@section page_Plot Data plot
The following plot was obtained with a random set of data 
@image html Plot.png width=600px

@section page_UI_src User Interface and Main Code Access
The link source in order to get the codes needed for the User Interface and the Main code as well as a csv. file with data for the plot provided are in the following link:
https://bitbucket.org/hjalbarr/me405_labs/src/master/Lab%204/





@page page_UIPhone User Interface with a Smart-phone code
The following codes provided in this section as well as the app are for the usage of a Nucleo-L476RG in which the user 
can interact with the LED on PinA5 for such board through an app design for an IOS device.
Through such app, the user can interact with the nucleo board and change the frequency at which the LED turns off and on
within the frequency values of 1-10 Hz.

@section page_FSMUIPhone FSM for UIPhone
The Finite State Machine (FSM) for the User Interface code is the following
@image html UIPhone.png width=600px

@section page_UIPhone_src User Interface Smart-phone code
The link source in order to get the code(s) for the user interface with a smart-phone can be found in the following link:
https://bitbucket.org/hjalbarr/me405_labs/src/master/Lab%205/ 

@section page_UIPhoneApp_src User Interface Smart-phone app
The link source for the app software for an IOs device can be found in the following link:
https://x.thunkable.com/copy/2bfc3645dc6356a0fab77f83420ff9e2







@page page_DCMotor DC Motor code
The following codes provided in this section are for the usage of a Nucleo-L476RG as well as a ME305/ME405 Balancing platform
in which an User can provide with different values of K_p and the code will modified the angular speed of the motor to a 
predetermined angular speed.

@section page_DCMotortrials DC Motor graphic trials
The following pictures represent different trial that were executed in order to get the code to work as well as refining the range for K_p 

@image html DC_1.png width=500px

@image html DC_2.png width=500px

@image html DC_3.png width=500px

@section page_DCMotor_src DC Motor source code
The following link is for the DC Motor codes necessary:
https://bitbucket.org/hjalbarr/me405_labs/src/master/Lab%206/









@page page_ReferenceTracking Reference Tracking codes
The following codes provided in this section are for the the performance of a velocity and position reference tracking for a given set of 
data that can be used with a Nucleo-L476RG as well as a ME305/ME405 balancing platform. Please refer to the codes for any reference regarding
the functioning of each task as well as the user interaction.

-------NOTE: Although both of the motors in the ME305/405 balancing platform were not able to function properly, one motor only
rotated one direction and the other wiggled at a rate that biased experimental data even more, the codes provided were designed to operate
at such given condition that would follow the reference tracking path provided, but the figures provided only follow the forward reference 
tracking path.

@section page_ReferenceTracking_trials Reference Tracking Figures for Trials
The following figures are some of the subplots obtained through the various iteration executed. 

@image html eight_plot.png width=500px

@image html sixth_plot.png width=500px

@image html Fourth_plot.png width=500px

@section page_ReferenceTracking_src Reference Tracking source code
The following link is for the necessary codes for Reference Tracking for Lab 0x07:
https://bitbucket.org/hjalbarr/me405_labs/src/master/Lab%207/codes/

@section page_ReferenceTracking_ExperientalData Reference Tracking Experimental Data
The following link is for the experimental data obtained from the figures provided in "Reference Tracking Figures for Trials:"
https://bitbucket.org/hjalbarr/me405_labs/src/master/Lab%207/Experimental_Data/

'''