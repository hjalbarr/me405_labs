'''
@file Fibonacci_sequency.py

@brief  This is a file which contains the Fibonacci_sequence code 

@details  The Fibonnaci sequence established in this document does not work with letters or sentences

@author  Horacio Albarran

'''

def fib(idx):
    ''' This method calculates a Fibonacci number corresponding to 
    a specified index provided that the index is within the parameters stablished.
    @param idx An integer specifying the index of the desired 
                Fibonacci number.'''
    
    an = str(idx)  # changing the input to an string so the input can be analized
    if an.isalpha() == True:    # To verify if input is a letter
        print ('The index should not be an alphabet letter(s), it should be a whole number')
    elif an.isnumeric() == False:  # To verify if input is an integer number
        print ('The index provided should be an integer number with no decimals')
    elif idx == 0:
        print (' The Fibonnaci value at index n=0 is 0')
    elif idx == 1:
        print (' The Fibonnaci value at index n=1 is 1')
    else:
        f_n=int(0)
        f_o=int(1)
        x=int(0)
        print ('Calculating the Fibonnaci number at  '
           'index n = {:}.'. format(idx))
        while x <= idx - int(1):
            x = x + int(1)
            x_0 = f_o 
            x_1 = f_n 
            f_n=int(x_0) + int(x_1)
            f_o=x_1
            print (f_n)
            
if __name__=='__main__':
    fib(10)
        