"""
@file ComputerInteraction.py

@details It contains a taksfile that collects data according to the readings from the
         Enconder connected to the Nucleo-L476RG 

Created on Tue Oct 20 12:10:42 2020

@author: horac
"""

import serial
import keyboard
import numpy as np
import matplotlib.pyplot as plt
import time

class UInterface:
    '''
    @brief It defines a class for the User Interface interaction
    
    '''
    
    # constant defining state 0
    S0_INIT               =  0
    
    # constant defining state 1
    S1_Update_Position    =  1
    
    
    def __init__(self, interval, keyboard, plt):
        '''
        @brief It creates an object for the User Interaction
        '''
        
        # It creates a variable for plt
        self.plt = plt
        
        # it initialized the serial port as well as defining it 
        self.ser = serial.Serial(port='COM3', baudrate=115273,timeout=1)
        
        # Initial state
        self.state = self.S0_INIT
        
        # It defines a variable for the "Global" variables
        self.keyboard = keyboard
        
        ## The timestamp for the first iteration
        self.start_time = time.time()    
        
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval    
        
        # It creates a list with the time position
        self.Time_list = []                
        
        # It creates a list with the encoder position
        self.Position_list = []
        
        
    def SendChar(self):
        '''
        @brief It creates an object which transforms the input to the ASCII corresponding value
        '''
        
        self.inv = input( 'Provide with a character:  ')
        self.ser.write(str(self.inv).encode('ascii'))
        
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def Plot(self):
        '''
        @brief It provides with the plot for the data provided
        '''
        self.plt.plot(self.Time_list, self.Position_list)
        self.plt.ylabel('Encoder position')
        self.plt.xlabel('Time (s)')
        
    def run(self): 
        '''
        @brief It will run the Computer Interaction code within Python
        '''
        while True:
            if self.state == self.S0_INIT:
                print ('A plot will be provided once the code is stopped. ')
                print('Allowable characters are:')
                print (' G = Collects data, S = Stop collection, and R = resets array')
                self.SendChar()
                self.curr_time = time.time()
                self.fut_time = self.curr_time + 10
                self.run = 0
                if self.inv == 'G':
                    while (self.curr_time <= self.fut_time):
                        self.curr_time += self.interval
                        self.run += 1
                        if self.keyboard.is_pressed('S'):
                            break
                        
                        else:
                            self.myval = self.ser.readline().decode('ascii')
                            self.Data = self.myval.strip().split(',')
                            self.Time = float(self.Data[1])
                            self.Position = float(self.Data[0])
                            self.Time_list.append(self.Time)
                            self.Position_list.append(self.Position)
                            print('run # ' + str(self.run))             
                            print(self.myval)
                            print('{:}, {:} '.format(self.Time_list, self.Position_list))
                            np.savetxt('lab4Time.csv', self.Time_list, delimiter=',')
                            np.savetxt('lab4Position.csv', self.Position_list, delimiter=',')
                    self.Plot()
                    self.transitionTo(self.S1_Update_Position)
                
                elif self.inv == 'S':
                    self.myval = self.ser.readline().decode('ascii')
                    self.Time_list.clear()
                    self.Position_list.clear()
                    print('\r\n')              
                    print(self.myval)
                    self.transitionTo(self.S1_Update_Position)
                    
                elif self.inv == 'R':
                    self.Time_list.clear()
                    self.Position_list.clear()
                    self.transitionTo(self.S1_Update_Position)
            
                else:
                    print('Invalid input')
                    self.transitionTo(self.S1_Update_Position)
                
            elif self.state == self.S1_Update_Position:
                self.SendChar()
                self.curr_time = time.time()
                self.fut_time = self.curr_time + 10
                self.run = 0
                if self.inv == 'G':
                    while (self.curr_time <= self.fut_time):
                        self.curr_time += self.interval
                        self.run += 1
                        if self.keyboard.is_pressed('S'):
                            break
                            
                        else:
                            self.myval = self.ser.readline().decode('ascii')
                            self.Data = self.myval.strip().split(',')
                            self.Time = float(self.Data[1])
                            self.Position = float(self.Data[0])
                            self.Time_list.append(self.Time)
                            self.Position_list.append(self.Position)
                            print('\r\n')                                       
                            print('run # ' + str(self.run))
                            print(self.myval)
                            print('{:} \r\n {:} '.format(self.Time_list, self.Position_list))
                            np.savetxt('lab4Time.csv', self.Time_list, delimiter=',')
                            np.savetxt('lab4Position.csv', self.Position_list, delimiter=',')
                    self.Plot()
                    self.transitionTo(self.S1_Update_Position)
                
                elif self.inv == 'S':
                    self.myval = self.ser.readline().decode('ascii')
                    self.Time_list.clear()
                    self.Position_list.clear()
                    print('\r\n')          
                    print(self.myval)
                    self.transitionTo(self.S1_Update_Position)
                    
                elif self.inv == 'R':
                    self.Time_list.clear()
                    self.Position_list.clear()
                    self.transitionTo(self.S1_Update_Position)
            
                else:
                    print('Invalid input')
                    self.transitionTo(self.S1_Update_Position)
                    
    
            

        
            
## Task object
task1 = UInterface(0.2, keyboard, plt)   # Will run a task for the the encoder

while True:    #Will change to   "while True:" once we're on hardware
   task1.run()
            
        

    
