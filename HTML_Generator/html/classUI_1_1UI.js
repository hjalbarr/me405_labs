var classUI_1_1UI =
[
    [ "__init__", "classUI_1_1UI.html#a296d3d3ed7fd6e6afda81301ece6a59f", null ],
    [ "LED", "classUI_1_1UI.html#a15122f26147ad9c31bbfc21b70f630b7", null ],
    [ "run", "classUI_1_1UI.html#a3c949e76e0a62c7152f783bcf5ea92d9", null ],
    [ "transitionTo", "classUI_1_1UI.html#a3c2cf94bad9151a6169b6726e6ecdfc6", null ],
    [ "curr_time", "classUI_1_1UI.html#a9fc054e5f7db6728fbb458a341cba779", null ],
    [ "DesireInterval", "classUI_1_1UI.html#aa5350b3edc67cb1a39159171f2e32bcb", null ],
    [ "InitialTimer", "classUI_1_1UI.html#a95fd9730ad41e3560d63e669ac75387a", null ],
    [ "interval", "classUI_1_1UI.html#a421ac2cdef36ffd01aa81394820ddbb9", null ],
    [ "myuart", "classUI_1_1UI.html#a792fe6110b94e49694ec8c8a81bdece9", null ],
    [ "next_time", "classUI_1_1UI.html#a495c8a06fc549836e5ce948d5f4b58f8", null ],
    [ "PinA5", "classUI_1_1UI.html#a783be35b34514a0a40eb6802f9e55321", null ],
    [ "runs", "classUI_1_1UI.html#a28b15e51a78bc7e55b434059771b1cff", null ],
    [ "start_time", "classUI_1_1UI.html#ae30281d2b69a0b79285306fbd989013a", null ],
    [ "state", "classUI_1_1UI.html#a8a7ec31d90f44f759b388453a93196c7", null ],
    [ "Time", "classUI_1_1UI.html#aea6609396e6cdfa701d7d2e6067a9b56", null ],
    [ "Timer", "classUI_1_1UI.html#ac5c45d39a08b849e9c8fc1bd82c6ff82", null ],
    [ "val", "classUI_1_1UI.html#a5db44265afb8f08e53e2140a4659b88f", null ]
];