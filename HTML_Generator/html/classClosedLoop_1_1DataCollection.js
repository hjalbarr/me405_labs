var classClosedLoop_1_1DataCollection =
[
    [ "__init__", "classClosedLoop_1_1DataCollection.html#aca342666ede57c33de31da6f5d3e0892", null ],
    [ "InputCheck", "classClosedLoop_1_1DataCollection.html#a48673e463f0b5728b2b2457851dbc28e", null ],
    [ "Percent", "classClosedLoop_1_1DataCollection.html#a94834e630f72aa874484523ffba0c44e", null ],
    [ "run", "classClosedLoop_1_1DataCollection.html#af7181a81e1e2d26bc00f81c6c9a65b8b", null ],
    [ "Ch1", "classClosedLoop_1_1DataCollection.html#a5546da38b50216c02fdd2a6392649bf7", null ],
    [ "Ch2", "classClosedLoop_1_1DataCollection.html#a20650c432344b8a77f425a9d6d8d201e", null ],
    [ "curr_time", "classClosedLoop_1_1DataCollection.html#af430ac24adb2c979500932674d14cc19", null ],
    [ "Duty_cycle", "classClosedLoop_1_1DataCollection.html#a2efe66c397588e93af71bfe05ba2a988", null ],
    [ "interval", "classClosedLoop_1_1DataCollection.html#a3d510aeec56430fd623609df1eae4fb9", null ],
    [ "modified_value", "classClosedLoop_1_1DataCollection.html#ab19ab352b4726b8f4e2c08f437f96a9e", null ],
    [ "myuart", "classClosedLoop_1_1DataCollection.html#a777688436cfb8210e5942940170a62da", null ],
    [ "next_time", "classClosedLoop_1_1DataCollection.html#af894e584c49b507a0ae94861f8e1a6a3", null ],
    [ "Omega_diff", "classClosedLoop_1_1DataCollection.html#ae8fcc26848bc6179075fc2de731b0fa1", null ],
    [ "raw_value", "classClosedLoop_1_1DataCollection.html#abde3802c537cd35737f34cac919ad405", null ],
    [ "runs", "classClosedLoop_1_1DataCollection.html#a73c1ef6f5c9d4d01bd9e6db9389d1d59", null ],
    [ "runs_old", "classClosedLoop_1_1DataCollection.html#a806146010298d3b0007535373c0781a1", null ],
    [ "start_time", "classClosedLoop_1_1DataCollection.html#a47977835b075329df104d50986ef7562", null ],
    [ "ticks_to_degrees", "classClosedLoop_1_1DataCollection.html#ac01bea03e8e7deec7a4d4695d47927d4", null ],
    [ "tim4", "classClosedLoop_1_1DataCollection.html#a14223ffb1d85a46b418a4e8178d2256b", null ],
    [ "Time", "classClosedLoop_1_1DataCollection.html#a653ed04644edece33087abc0f3e0dd0c", null ]
];