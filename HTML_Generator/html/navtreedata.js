/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Horacio's ME 305 Workbook", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Fibonacci code (lab 0x01)", "index.html#sec_Fibonacci_code", null ],
    [ "Elevator code", "index.html#sec_Elevator_code", null ],
    [ "LED code (lab 0x02b)", "index.html#sec_LED_code", null ],
    [ "Encoder code (lab 0x03)", "index.html#sec_Encoder_code", null ],
    [ "User Interface code (lab 0x04)", "index.html#sec_UI_code", null ],
    [ "User Interface Smart-phone code (lab 0x05)", "index.html#sec_UIPhone_code", null ],
    [ "DC Motor code (lab 0x06)", "index.html#sec_DCMotor_code", null ],
    [ "Reference Tracking code (lab 0x07)", "index.html#sec_ReferenceTracking_code", null ],
    [ "Fibonacci task", "page_fibonnaci.html", [
      [ "Introduction", "page_fibonnaci.html#page_fibonacci_Introduction", null ],
      [ "Fibonacci Source Code Access", "page_fibonnaci.html#page_fibonacci_src", null ]
    ] ],
    [ "Elevator task", "page_elevator.html", [
      [ "Introduction", "page_elevator.html#page_elevator_Introduction", null ],
      [ "FSM for Elevator code", "page_elevator.html#page_elevator_FSM", null ],
      [ "Elevator Source Code Access", "page_elevator.html#page_elevator_src", null ]
    ] ],
    [ "LED Blinking task", "page_LED.html", [
      [ "Introduction", "page_LED.html#page_LED_Introduction", null ],
      [ "FSM LED", "page_LED.html#page_LED_FSMLED", null ],
      [ "LED Source Code Access", "page_LED.html#page_LED_src", null ]
    ] ],
    [ "Encoder and User Interface task", "page_Encoder.html", [
      [ "Introduction", "page_Encoder.html#page_Encoder_Introduction", null ],
      [ "FSM Encoder", "page_Encoder.html#page_FSMEncoder", null ],
      [ "FSM User Interface", "page_Encoder.html#page_FSMUserInterface", null ],
      [ "Encoder Source Code Access", "page_Encoder.html#page_Encoder_src", null ]
    ] ],
    [ "User Interface and Main code", "page_UI.html", [
      [ "Introduction", "page_UI.html#page_UI_Introduction", null ],
      [ "FSM for Main code", "page_UI.html#page_FSMMain", null ],
      [ "FSM for User Interface", "page_UI.html#page_FSMUI", null ],
      [ "Data plot", "page_UI.html#page_Plot", null ],
      [ "User Interface and Main Code Access", "page_UI.html#page_UI_src", null ]
    ] ],
    [ "User Interface with a Smart-phone code", "page_UIPhone.html", [
      [ "FSM for UIPhone", "page_UIPhone.html#page_FSMUIPhone", null ],
      [ "User Interface Smart-phone code", "page_UIPhone.html#page_UIPhone_src", null ],
      [ "User Interface Smart-phone app", "page_UIPhone.html#page_UIPhoneApp_src", null ]
    ] ],
    [ "DC Motor code", "page_DCMotor.html", [
      [ "DC Motor graphic trials", "page_DCMotor.html#page_DCMotortrials", null ],
      [ "DC Motor source code", "page_DCMotor.html#page_DCMotor_src", null ]
    ] ],
    [ "Reference Tracking codes", "page_ReferenceTracking.html", [
      [ "Reference Tracking Figures for Trials", "page_ReferenceTracking.html#page_ReferenceTracking_trials", null ],
      [ "Reference Tracking source code", "page_ReferenceTracking.html#page_ReferenceTracking_src", null ],
      [ "Reference Tracking Experimental Data", "page_ReferenceTracking.html#page_ReferenceTracking_ExperientalData", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classUI_1_1UI.html#a95fd9730ad41e3560d63e669ac75387a"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';