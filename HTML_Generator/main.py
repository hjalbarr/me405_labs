"""
@file main.py

@details It is a file for the Interaction of an User Interface which is controled through Python


Created on Tue Oct 20 12:49:45 2020

@author: horac
"""
import utime
import pyb
from pyb import UART

class DataCollection:
    '''
    @brief It creates a class that will provide information about the Encoder
    @details It will create a class that will compute information about the Encoder
             position as well as communicating with the Computer Interaction code
             in order to provide with data for the User
    '''
    
    # constant defining state 0
    S0_INIT               =  0
    
    # constant defining state 1
    S1_Update_Position    =  1
    
    def __init__(self, interval):
        '''
        @brief It creates an object for the DataCollection class
        '''
        
        # It initialize the baudrate
        self.myuart = UART(2)
        
        # It initiates the "val" variable
        self.val = 0
        
        # it creates the initial state
        self.state = self.S0_INIT
        
         # Defining the tim variable for the timer used
        self.timer = pyb.Timer(3)
    
        # Provides with the prescalar and period for the input variable
        self.timer.init(prescaler=0, period=0XFFFF)
        
        # It defines the corresponding properties for channel 1
        self.Ch1 = self.timer.channel (1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
        
        # It defines the corresponding properties for channel 2
        self.Ch2 = self.timer.channel (2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()    
        
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)                             # In this form, 1 interval = 1 second     
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        # It updates the value of the Encoder
        self.En_Update = self.timer.counter()
        
    
        
    
    def InputCheck(self):
        '''
        @brief It will check the input of the variable provided in order to execute the appropiate task
        '''
        
        if self.myuart.any() != 0:
            self.runs_old = self.runs + 100     # It sets a varible with the old number of runs when the input was received
            self.runs_old2 = self.runs + 1     # It defines a varible to stop data collection
            self.val = self.myuart.readchar()

        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def Update(self):
        '''
        @brief It returns the updated position of the encoder
        '''
        return self.timer.counter()
        
    def get_position(self):
        '''
        @brief It creates an object with the most recent properties of the encoder
        
        '''
        # it defines the updated value for the input provided by the Nucleo
        self.En_Position = self.En_Update
        #str = 'P{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.En_Position, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
        #print(str)

        
    def run(self):
        '''
        @brief It creates an object through which the DataCollection class will run
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
           if (self.state == self.S0_INIT):
               self.InputCheck()
               if self.val == 71:                                   # It will collect Data if true
                   if self.runs < self.runs_old:            # It sets a period of 10 seconds for the collectiong of data
                           self.Data = (self.Update())     # it will set the next index of the array to the value of the encoder
                           self.Time = ((utime.ticks_diff(self.curr_time,self.start_time))*1e-6)    # It adds the time in seconds to the time array 
                           self.myuart.write('{:}, {:}\r\n'.format(self.Data, self.Time))
                           self.transitionTo(self.S1_Update_Position) 
                           
                   elif self.runs == self.runs_old:
                       self.val = 0

               elif self.val == 83:                                 # It will stop Data Collection if true
                   if self.runs < (self.runs_old2):
                       self.Data = 0
                       self.Time = 0
                       self.myuart.write('{:}, {:}\r\n'.format(self.Data, self.Time))
                       self.transitionTo(self.S1_Update_Position)
               
               else:
                   self.transitionTo(self.S1_Update_Position)

           elif (self.state == self.S1_Update_Position):
               self.InputCheck()
               if self.val == 71:                                   # It will collect Data if true
                   if self.runs < (self.runs_old):             # It sets a period of 10 seconds for the collectiong of data
                           self.Data = (self.Update())     # it will set the next index of the array to the value of the encoder
                           self.Time = ((utime.ticks_diff(self.curr_time,self.start_time))*1e-6)    # It adds the time in seconds to the time array     
                           self.myuart.write('{:}, {:}\r\n'.format(self.Data, self.Time))
                           self.runs +=1
                  
                   elif self.runs == self.runs_old:
                       self.val = 0
                           
               elif self.val == 83:                                 # It will stop Data Collection if true
                   if self.runs < (self.runs_old2):
                       self.Data = 0
                       self.Time = 0
                       self.myuart.write('{:}, {:}\r\n'.format(self.Data, self.Time)) 
                    
    
           # It adds one to the runs
           self.runs += 1
    
           # Specifying the next time the task will run
           self.next_time = utime.ticks_add(self.next_time, self.interval)
        
        
                
                
                
## Task object
task1 = DataCollection(0.2)   # Will run a task for the the encoder

while True:    #Will change to   "while True:" once we're on hardware
   task1.run()  
        
                


        
                
                
