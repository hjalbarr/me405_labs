'''
@file Elevator_exercise_main_sec_02.py

'''

from Elevator_exercise import Button, MotorDriver, TaskElevator, Floor
        
## Motor Object for first elevator
Motor = MotorDriver()

## Button Object to identify if elevator is in second floor
Second = Floor ('PB6')

## Button Object to identify if elevator is in first floor
First = Floor ('PB7')

## Button Object to go to the first floor
Button_1 = Button('PB8')

## Button Object to go to the second floor
Button_2 = Button('PB9')

## Motor Object for second elevator
Motor_2 = MotorDriver()

## Button Object to identify if second elevator is in second floor
Second_2 = Floor ('PB10')

## Button Object to identify if second elevator is in first floor
First_2 = Floor ('PB11')

## Button Object to go to the first floor in the second elevator
Button_1_2 = Button('PB12')

## Button Object to go to the second floor in the second elevator
Button_2_2 = Button('PB13')

## Task object
task1 = TaskElevator(.1, Motor, Second, First, Button_1, Button_2) # Will run a task for the first elevator
task2 = TaskElevator(0.5, Motor_2, Second_2, First_2, Button_1_2, Button_2_2) # Will run a task for the second elevator

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
    task2.run()
#    task3.run()



