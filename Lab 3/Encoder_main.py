"""
@file  Encoder_main.py
@details A container for the task that need to be run 
Created on Tue Oct 13 14:20:10 2020

@author: horac
"""
import Shares
from Encoder import TaskEncoder
from Encoder import User_Interface

## Task object
task1 = TaskEncoder(1, Shares)   # Will run a task for the the encoder
task2 = User_Interface(Shares) # Will run a task for the User_interface



while True:    #Will change to   "while True:" once we're on hardware
   task1.run()
   task2.run()
#    task3.run()