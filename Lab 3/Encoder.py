
"""
@file Encoder.py

This file will provide with a code in which some of the properties of the Encoder
will be define as well as interacting with a User with some specific commands 
that will either allow the user to:
    z = Zero the encoder position
    p = Print out the encoder position
    d = Print out delta of the encoder

Created on Tue Oct 13 13:24:42 2020

@author: horac
"""

import utime  
import pyb 
import Shares
from pyb import UART

class TaskEncoder:
    '''
    @brief This will encapsulate the operation of the timer to read from an encoder
    @details This class will generate cetain properties of an Encoder connected to a
                 Nucleo-L476RG
    '''
    
    # constant defining state 0
    S0_INIT               =  0
    
    # constant defining state 1
    S1_Update_Position    =  1
    
    ## Constant defining State 2
    S2_Update_Position    =  2
    
    
    def __init__(self, interval, Shares):     
        '''
        @brief creates an encoder object
        
        '''
        
        # It defines a variable that can be used in this class which uses the variables defined in Shares.py
        self.Shares = Shares  
        
        # Defining the tim variable for the timer used, we used Timer(3)
        self.timer = pyb.Timer(3)
    
        # Provides with the prescalar and period for the input variable
        self.timer.init(prescaler=0, period=0XFFFF)
        
        # It defines the corresponding properties for channel 1
        self.Ch1 = self.timer.channel (1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
        
        # It defines the corresponding properties for channel 2
        self.Ch2 = self.timer.channel (2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)
        
        # The state to run in the beginnig of the iteration
        self.state = self.S0_INIT
    
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The time since 2000-01-01
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)         
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def update (self):  
        '''
        @brief It creates an object which contains the recent recorded position of the Encoder
        '''
        # it updates the value of the Encoder
        self.Shares.En_Update   = self.timer.counter()
       
    def get_position(self):
        '''
        @brief It creates an object with the most recent update position of the Encoder and prints it
        
        '''
        # it defines the updated value for the input provided by the Nucleo
        self.En_Position = self.Shares.En_Update
        print (' The Updated value for the counter is {:0.2f}'.format(self.En_Position))
        
    def set_position(self, OldReading):
        '''
        @brief It create an object which contains the old position of the Encoder
        '''
        
        self.En_old = OldReading   # It provides with the old reading from the encoder to the individual states
        
    def get_delta(self):
        '''
        @brief It creates an object the provides with the difference in recorded positions
        '''
        self.Delta_Check = self.Shares.En_Update - self.En_old   # It provides with the raw delta value to be check whether is good or bad
        if self.Delta_Check > 0:                                # Checks if Delta_good is true
            self.Shares.Delta = self.Delta_Check
            print ('The delta value is {:0.2f}'.format(self.Shares.Delta))
        elif self.Delta_Check < 0:                               # Checks if Delta_bad is true and corrects it
            self.Delta_Check  = self.Shares.En_Update + self.En_old
            self.Shares.Delta = self.Delta_Check
            print ('The delta value is {:0.2f}'.format(self.Shares.Delta))
        else:
            print('Delta is zero')
        
    def run(self):
        '''
        @brief   It runs the iteration for the Encoder input
        @details It will continuosly provided with the input provided by the 
                 encoder motor
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
       
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                self.En_old = 0                    # It defines the initial old reading of the Encoder
                self.transitionTo(self.S1_Update_Position)
            elif (self.state == self.S1_Update_Position):
                self.next_time = utime.ticks_add(self.start_time, self.interval)
                self.update()                      # It updates the position of the encoder for state 1
                if self.Shares.En_Update != self.En_old:    # Check if the value of the Encoder has changed
                    self.get_position()                # It prints the recent position of the Encoder for State 1
                    self.get_delta()
                    self.set_position(self.Shares.En_Update)  # Setting old reading of Encoder for state 1 based on the Encoder reading of state 2
                    self.transitionTo(self.S2_Update_Position)
                else:
                    self.transitionTo(self.S2_Update_Position)
            elif (self.state == self.S2_Update_Position):
                self.next_time = utime.ticks_add(self.start_time, self.interval)
                self.update()
                if self.Shares.En_Update != self.En_old:   # Checks if the value of the Encoder has changed
                    self.get_position()
                    self.get_delta()
                    self.set_position(self.Shares.En_Update)
                    self.transitionTo(self.S1_Update_Position)
                else:
                    self.transitionTo(self.S1_Update_Position)
            else:
                print ( 'The input provided is not recognized')
                
                
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
        
        

class User_Interface:
    '''
    @brief This class will provide with the result for the User Interface input
    @details This class allows the user to interact with the code by providing
            certain commands that will yield with some of the properties of 
            the Encoder as defined in the introduction of this file
    '''
    
    # constant defining state 0
    S0_INIT            =  0
    
    # constant defining state 1
    S1_Update_Input    =  1
    
    ## Constant defining State 2
    S2_Update_Input    =  2
    
    def __init__(self, Shares):
        '''
        @brief It creates an object function which allows a user to interact with the code
        '''
        # It creates a variable with the Shares variables encapsule in it
        self.Shares = Shares
        
        # init with a given baudrate
        self.uart = pyb.UART(2)
    
        # The state to run in the beginnig of the iteration
        self.state = self.S0_INIT
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
    def Allowable_Input(self):
        '''
        @brief It creates an object with the allowable User inputs
        '''
        print (' The possible inputs are, notice they all are lower case letters: ')
        print ( 'z = Zero the Encoder Position')
        print ( 'p = Print out the Encoder position ')
        print ( 'd = Print out delta of the Encoder')
        
    def run(self):
        '''
        @brief   It runs the the User_Interface class
        @details It will continuosly check for an input provided by the user
        '''
        
        self.Input = self.uart.any()     #It will check for any input provided by the user
       
        if self.Input:
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S1_Update_Input)          
            elif (self.state == self.S1_Update_Input):
                self.Value = self.uart.readchar()
                if self.Value == 100:                                   # Checks if the input provided is "d"
                    print (' The Encoder delta value is {:0.2f}'.format(self.Shares.Delta))
                    self.transitionTo(self.S2_Update_Input)
                elif self.Value == 122:                                 # Checks if the input provided is "z"
                    self.Shares.En_Update = 0
                    print ( ' The Encoder position has been set to {:0.2f}'.format(self.Shares.En_Update))
                    self.transitionTo(self.S2_Update_Input)
                elif self.Value == 112:                                 # Checks if the input provided is "p"
                    print ( ' The Encoder position is {:0.2f}'.format(self.Shares.En_Update))
                    self.transitionTo(self.S2_Update_Input)
                else:
                    print ( 'Please refer to the allowable inputs')
                    self.Allowable_Input()
                    self.transitionTo(self.S2_Update_Input)
            elif (self.state == self.S2_Update_Input):
                self.Value = self.uart.readchar()
                if self.Value == 100:
                    print (' The Encoder delta value is {:0.2f}'.format(self.Shares.Delta))
                    self.transitionTo(self.S1_Update_Input)
                elif self.Value == 122:
                    self.Shares.En_Update = 0 
                    print ( ' The Encoder position has been set to {:0.2f}'.format(self.Shares.En_Update))
                    self.transitionTo(self.S1_Update_Input)
                elif self.Value == 112:
                    print ( ' The Encoder position is {:0.2f}'.format(self.Shares.En_Update))
                    self.transitionTo(self.S1_Update_Input)
                else:
                    print ( 'Please refer to the allowable inputs')
                    self.Allowable_Input()
                    self.transitionTo(self.S1_Update_Input)    
            else:
                print ('Undefined input')  
        else:
            pass
                
                
                
                
        
        
        
