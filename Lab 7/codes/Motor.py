"""
@file Motor.py
Created on Wed Nov 11 14:55:21 2020

@author: horac
"""

import pyb

class MotorDriver:
    '''
    @brief This class implements a motor driver for the ME405 board.
    '''
    
    
    def __init__(self):
        '''
        @brief Creates a motor driver by initiliazing GPIO
        '''
        
        # It sets a variable for the pin nSLEEP 
        self.nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        
        # It initialize the timer as well as its frequency
        self.tim3   = pyb.Timer(3, freq=20000)
        
        # It defines the variable for the negative PWM output signal for motor_1
        self.IN1  = pyb.Pin(pyb.Pin.cpu.B4)
        self.IN2  = pyb.Pin(pyb.Pin.cpu.B5)
        
        # It defines the varibale for the positive PWM output signal for motor_2
        self.IN3  = pyb.Pin(pyb.Pin.cpu.B0)
        self.IN4  = pyb.Pin(pyb.Pin.cpu.B1)
        
        # It defines the channel for the negative PWM output signal for motor_1
        self.t3ch1  = self.tim3.channel(1, pyb.Timer.PWM, pin=self.IN1)
        self.t3ch2  = self.tim3.channel(2, pyb.Timer.PWM, pin=self.IN2)
        
        # It defines the channel for the positve PWM output signal for motor_2
        self.t3ch3  = self.tim3.channel(3, pyb.Timer.PWM, pin=self.IN3)
        self.t3ch4  = self.tim3.channel(4, pyb.Timer.PWM, pin=self.IN4)
        
        
    def enable(self):
        '''
        @brief It defines the varible of nSLEEP as high so the motors can function
        '''
        
        self.nSLEEP.high()
        #print('Enabling Motors')
        
        
    def disable(self):
        '''
        @brief It defines the variable of nSLEEP as low so no signal can make the motors function
        '''
        
        self.nSLEEP.low()
        #print('Disabling Motor')
        
    def set_duty(self, Duty_Cycle):
        '''
        @details This method sets the duty cycle to be sent to the motor to the given level.
                 Positive values cause the effort in one direction, and negative values in the
                 opposite direction.
        '''
        
        self.percent = Duty_Cycle    # It only runs for a duty_percent of 30% and above provided by "Shares.Duty_cycle"
        #print(self.percent)
        
        if (self.percent > 0):                       
            #print('Motor is rotating counterclockwise at ' + str(self.percent))
            
            # It sets motor_1 to rotate counterclockwise
            self.t3ch2.pulse_width_percent(0)
            self.t3ch1.pulse_width_percent(self.percent)

        elif (self.percent < 0):                   
            self.percent2 = -self.percent                     # It changes the sign so the value can be analized
            #print('Motor is rotating clockwise at ' + str(self.percent2))
            
            # It sets motor_2 to rotate clockwise
            # self.t3ch4.pulse_width_percent(self.percent2)
            # self.t3ch3.pulse_width_percent(0)
            # It sets motor_1 to rotate clockwise
            self.t3ch2.pulse_width_percent(self.percent2)
            self.t3ch1.pulse_width_percent(0)
            
        elif (self.percent) == 0:                             # Motor is coasting
        
            self.t3ch2.pulse_width_percent(0)
            self.t3ch1.pulse_width_percent(0)
            
            
   
        
    


