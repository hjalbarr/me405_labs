"""
@file ComputerInteraction2.py

@details It contains a taksfile that collects data according to the readings from the
         Enconder connected to the Nucleo-L476RG 

Created on Tue Oct 20 12:10:42 2020

@author: horac
"""

import serial
import keyboard
import numpy as np
import matplotlib.pyplot as plt
import time

class UInterface:
    '''
    @brief It defines a class for the User Interface interaction
    
    '''
    
    # constant defining state 0
    S0_CHECK               =  0
    
    # constant defining state 1
    S1_Update_Position    =  1
    
    
    def __init__(self):
        '''
        @brief It creates an object for the User Interaction
        '''
        
        # It sets a local variable to use numpy
        self.np = np
        
        # It sets a local variable to use the plot command
        self.plt = plt
        
        # It sets the variable to run the code once
        self.inv = 'G'
        
        # it initialized the serial port as well as defining it 
        self.ser = serial.Serial(port='COM7', baudrate=115273,timeout=1)
        
        # Initial state
        self.state = self.S0_CHECK
        
        # It defines a variable for the "Global" variables
        self.keyboard = keyboard
        
        ## The timestamp for the first iteration
        self.start_time = time.time()    
        
        ## The interval of time, in seconds, between runs of the task
        self.interval = 12e-3                         # Units of miliseconds, 25 and 10, 15 AND 8 was good, 5 and 4 was also good
        
        # It creates a list with the time position
        self.Time_list = []                
        
        # It creates a list with the omega_ref value
        self.Omega_ref_list = []
        
        # It creates a varible for the values of omega
        self.Omega_list = []
        
        # It creates an array with the time and position in it 
        self.Data_list = []
        
        # It sets a list for the position measured values obtained
        self.Position_list = []
        
        # It sets a list for the position reference provided by the csv file
        self.Position_ref_list = []
        
        # It creates a list for the performances values
        self.Performance_list = []
        
        # To read the position of the encoder through python
        self.ref = open('ref.csv');
        
    def SendChar(self):
        '''
        @brief It creates an object which transforms the input to the ASCII corresponding value
        '''
        print('\r\n')
        print('Input condition can either be 0=Off or 1=On to run iteration')
        print('Press "S" to stop or "P" to plot and provide with a new command')
        print('while collecting data')
        self.Condition = input('Provide with input for desired condition:  ')
        self.ser.write(str(self.Condition).encode('ascii'))
        
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def Plot(self):
        '''
        @brief It provides with the plot for the data provided
        '''
        self.plt.figure()
        self.plt.subplot(2,1,1)
        Experimental, = self.plt.plot(self.Time_list, self.Omega_list)
        Theory, = self.plt.plot(self.Time_list, self.Omega_ref_list)                     # Plotting omega_ref within the same graph
        self.plt.legend([Theory, Experimental], ['Theory', 'Experimental'])
        self.plt.title ('Lab 0x07')
        self.plt.ylabel('Ω (RPM)')
        self.plt.xlabel('Time (s)')
        self.plt.grid()
        self.plt.subplot(2,1,2)
        Experimental2, = self.plt.plot(self.Time_list, self.Position_list)                           # It plots the measured position from the encoders
        Theory2, = self.plt.plot(self.Time_list, self.Position_ref_list)
        self.plt.legend([Theory2, Experimental2], ['Theory', 'Experimental'])
        #self.plt.title ('Position vs Time')
        self.plt.ylabel('θ (degree)')
        self.plt.xlabel('Time (s)')
        self.plt.grid()
        self.plt.show()
        
        
    def run(self): 
        '''
        @brief It will run the Computer Interaction code within Python
        '''
        while True:
            if self.state == self.S0_CHECK:
                self.SendChar()
                self.curr_time = time.time()
                self.fut_time = self.curr_time + 15                    # It runs state_1; self.curr_time + 24 is arround 15 seconds 
                self.next_time = self.interval + self.curr_time        # It sets the iteration interval for state_1
                self.run = 0
                if self.inv == 'G':                                    # To keep collecting data
                    self.transitionTo(self.S1_Update_Position)
                
                elif self.inv == 'S':                                  # To start a new set of data
                    self.myval = self.ser.readline().decode('ascii')
                    self.Time_list.clear()
                    self.Position_list.clear()
                    print('\r\n')              
                    print(self.myval)                                 # Prints the last value obtained from the data list
                    self.Plot()
                    self.transitionTo(self.S1_Update_Position)
                    
                elif self.inv == 'P':                                 # To plot the data obtained
                    self.Plot()
                    self.transitionTo(self.S1_Update_Position)
            
                else:
                    print('Invalid input')
                    self.transitionTo(self.S1_Update_Position)
                
            elif self.state == self.S1_Update_Position:
                    self.curr_time = time.time()                   # It provides with the current time
                    if (self.curr_time <= self.fut_time):          # It sets the parameter to run for the amount of time specified in S0_init, as for now, it is 15 seconds
                        #self.curr_time += self.interval
                        self.run += 1
                        if self.keyboard.is_pressed('S'):             # To stop data collection and input a new command
                            self.inv = 'S'
                            self.transitionTo(self.S0_CHECK)
                            
                        elif self.keyboard.is_pressed('P'):           # To stop data collection and plot the data obtained thus far
                            self.inv = 'P'
                            self.transitionTo(self.S0_CHECK)
                            
                        else: 
                            if ((self.curr_time - self.next_time) > 0):                        # It sets the intervals for the iterations     
                                self.next_time = self.interval + self.curr_time
                                self.myval = self.ser.readline().decode('ascii')
                                self.Data = self.myval.strip().split(',')
                                print(self.Data)
                                self.Performance = float(self.Data[3])                          
                                #self.Position_ref = float(self.Data[4])                        # To read the position from the csv from the nucleo
                                self.Position_ref = float(self.ref.readline())                       # To read the position from the csv from python
                                self.Position = float(self.Data[3])                                 # It provides with the performance value for the motor
                                self.Omega_ref = float(self.Data[2])                          # It provides with the position of the motor based on the encoder
                                self.Time = float(self.Data[1])                              # Time provided by PuTTY
                                #self.Time = ((self.curr_time - self.start_time)*1e-3)    # It provides with the time given by python script in microseconds  
                                self.Omega = float(self.Data[0])                             # It provides with the omega_measured value
                                #print('{:},{:},{:},{:},{:0.3f}'.format(self.Position_ref,self.Position,self.Omega_ref,self.Omega,self.Time))
                                self.Performance_list.append(self.Performance)
                                self.Position_ref_list.append(self.Position_ref)
                                self.Position_list.append(self.Position)
                                self.Omega_ref_list.append(self.Omega_ref)
                                self.Time_list.append(self.Time)
                                self.Omega_list.append(self.Omega)
                                self.Data_list = [self.Omega_list, self.Omega_ref_list, self.Time_list, self.Position_list, self.Position_ref_list, self.Performance_list]
                                self.Data_list2 = self.np.transpose(self.Data_list)                                       
                                print('run # ' + str(self.run))
                                #print(self.Position)
                                #print(self.Data_list)
                                np.savetxt('lab7Data.csv', self.Data_list2, delimiter=',')         
                                self.transitionTo(self.S1_Update_Position)
                            
                    else:
                        self.inv = 'G'
                        self.Plot()
                        self.transitionTo(self.S0_CHECK)
                    
    ## If it not working, check additon of Position of encoder
            

        
            
## Task object
task1 = UInterface()   # Will run a task for the the encoder

while True:    #Will change to   "while True:" once we're on hardware
   task1.run()
            
        

    
