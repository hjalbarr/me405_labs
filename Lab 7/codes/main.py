"""
@file  main.py
@details A container for the tasks that need to be run 

@author: horac
"""
import Shares
from EncoderDriver import TaskEncoder
from ClosedLoop import DataCollection
from Motor import MotorDriver
import utime
from pyb import UART

class main:
    '''
    @brief It contains the main file to run all the necessary task for lab#7
    '''

    # Constant defining state_0
    S0_init = 0
    
    # Constant defining state_1
    S1_run  = 1

    def __init__(self):
        '''
        @brief It will initialiaze the variables on the main file
        '''
        
        # It sets the initial state
        self.state = self.S0_init
        
        # Variable for the EncoderDriver code
        self.EncoderDriver = TaskEncoder()  
        
        # Variable for the ClosedLoop code
        self.ClosedLoop = DataCollection() 
        
        # Variable for the Motor code
        self.Motor = MotorDriver() 
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()    
        
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(8e3)     # real time for the code, 1e6=1-second       # Selfnote, 8e3, 4e3 intervals works fine
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        # It set a variable with the number of runs
        self.runs = 0
        
        # It initialize the baudrate
        self.myuart = UART(2)
        
        # Local variable for the summation of changes in omega and tetha
        self.summation = 0
        
        # It opens the csv file with the encoder position reference values (if desired)
        self.ref = open('ref.csv')
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    def Deltas(self):
        '''
        @brief It creates a function to calculate the increments of the velocity
                and tetha
        '''
        # It gets the summation of the difference of velocities 
        self.delta_velocity = (Shares.Omega_ref - Shares.Omega_measured)
        
        # It gets the summation for all delta_tetha values
        self.delta_tetha = (Shares.Delta)
        
        self.summation += ((int(self.delta_velocity))^2 + (int(self.delta_tetha))^2)
        
    def Performance(self):
        '''
        @brief It creates a function for the perfomance function
        '''
        
        Shares.J = (int(1)/self.runs)*(self.summation)

    def run(self):
        '''
        @brief it will run the main file according to the input provided
        ''' 
        
        if self.state == self.S0_init: 
            #Shares.Condition = 1
            if Shares.Check:
                if Shares.Condition == 1:                                              # It will start running the iteration on State_1
                    self.Motor.enable()                                                # Enables the motor
                    self.ref = open('ref.csv')                                         # It opens the csv file to run another iteration (if desired to be used)
                    Shares.start_time = utime.ticks_us()
                    #self.next_time = utime.ticks_add(self.start_time, self.interval)
                    self.transitionTo(self.S1_run)
                        
                elif Shares.Condition == 0:                                            # It will not run the code
                    self.Motor.disable()                                               # Disables the motor
                    self.ClosedLoop.run()
                    self.transitionTo(self.S0_init)
                         
            else:
                #self.ClosedLoop.run()
                self.ClosedLoop.InputCheck()
                self.transitionTo(self.S0_init)
                
        elif self.state == self.S1_run:
            self.ClosedLoop.InputCheck()
            if Shares.Check:                                                           # Checks if a new input has been provided
                # Initiliaze each variable to start a new trial
                self.ref.close()
                Shares.Position = 0
                Shares.Duty_cycle = 0
                Shares.Omega_ref = 30                                                  # Units of RPM
                Shares.Omega_measured = 0    
                Shares.K_p = 3.3
                Shares.J = 0
                self.transitionTo(self.S0_init)
                
            else:                                                                          # If no input has been provided, it runs the iteration
                self.curr_time = utime.ticks_us()                                          # Updating the current timestamp
                if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
                    self.next_time = utime.ticks_add(self.next_time, self.interval)        # It sets the time at which the next iteration will be run
                    self.runs += 1
                    Shares.Position_ref = self.ref.readline()                              # To read the reference value from the csv file
                    if (0 <= ((utime.ticks_diff(self.curr_time, Shares.start_time))) < 4e6):
                        Shares.Omega_ref = 30                                          # This value of omega provides with a value for omega of 30 RPM with the balancing platform provided
                        self.ClosedLoop.run()                                          # To collect the data from the encoder
                        self.Motor.set_duty(Shares.Duty_cycle)                         # To set percent of duty cycle on motor
                        self.EncoderDriver.run()                                       # To get the omega reference from encoder
                        self.Deltas()                                                  # To calculate the deltas for velocity and tetha
                        self.Performance()                                             # It gets the performance 
                        self.transitionTo(self.S1_run)
                        
                    elif (4e6 <= ((utime.ticks_diff(self.curr_time, Shares.start_time))) < 7e6):
                        Shares.K_p = 0
                        Shares.Omega_ref = 0.0 
                        self.ClosedLoop.run()                                          # To collect the data from the encoder
                        self.Motor.set_duty(Shares.Duty_cycle)                         # To set percent of duty cycle on motor
                        self.EncoderDriver.run()                                       # To get the omega reference from encoder
                        self.Deltas()                                                  # To calculate the deltas for velocity and tetha
                        self.Performance()                                             # It gets the performance 
                        self.transitionTo(self.S1_run)
                        
                    elif (7e6 <= ((utime.ticks_diff(self.curr_time, Shares.start_time))) < 8e6):
                        Shares.K_p = 2.5
                        Shares.Omega_ref = -60
                        self.ClosedLoop.run()                                          # To collect the data from the encoder
                        self.Motor.set_duty(Shares.Duty_cycle)                         # To set percent of duty cycle on motor
                        self.EncoderDriver.run()                                       # To get the omega reference from encoder
                        self.Deltas()                                                  # To calculate the deltas for velocity and tetha
                        self.Performance()                                             # It gets the performance 
                        self.transitionTo(self.S1_run)
                    
                    elif (8e6 <= ((utime.ticks_diff(self.curr_time, Shares.start_time))) < 11e6):
                        Shares.K_p = 3.3
                        Shares.Omega_ref = 0 
                        self.ClosedLoop.run()                                          # To collect the data from the encoder
                        self.Motor.set_duty(Shares.Duty_cycle)                         # To set percent of duty cycle on motor
                        self.EncoderDriver.run()                                       # To get the omega reference from encoder
                        self.Deltas()                                                  # To calculate the deltas for velocity and tetha
                        self.Performance()                                             # It gets the performance 
                        self.transitionTo(self.S1_run)
                        
                    elif (11e6 <= ((utime.ticks_diff(self.curr_time, Shares.start_time))) < 12e6):
                        Shares.K_p = 3.3
                        Shares.Omega_ref += 0.03
                        self.ClosedLoop.run()                                          # To collect the data from the encoder
                        self.Motor.set_duty(Shares.Duty_cycle)                         # To set percent of duty cycle on motor
                        self.EncoderDriver.run()                                       # To get the omega reference from encoder
                        self.Deltas()                                                  # To calculate the deltas for velocity and tetha
                        self.Performance()                                             # It gets the performance  
                        self.transitionTo(self.S1_run)
                        
                    elif (12e6 <= ((utime.ticks_diff(self.curr_time, Shares.start_time))) < 13e6):
                        Shares.K_p = 3.3
                        Shares.Omega_ref -= 0.03
                        self.ClosedLoop.run()                                          # To collect the data from the encoder
                        self.Motor.set_duty(Shares.Duty_cycle)                         # To set percent of duty cycle on motor
                        self.EncoderDriver.run()                                       # To get the omega reference from encoder
                        self.Deltas()                                                  # To calculate the deltas for velocity and tetha
                        self.Performance()                                             # It gets the performance
                        self.transitionTo(self.S1_run)
                        
                    elif (13e6 <= ((utime.ticks_diff(self.curr_time, Shares.start_time))) < 15e6):
                        Shares.Omega_ref = 0
                        Shares.K_p = 0 
                        self.ClosedLoop.run()                                          # To collect the data from the encoder
                        self.Motor.set_duty(Shares.Duty_cycle)                         # To set percent of duty cycle on motor
                        self.EncoderDriver.run()                                       # To get the omega reference from encoder
                        self.Deltas()                                                  # To calculate the deltas for velocity and tetha
                        self.Performance()                                             # It gets the performance 
                        self.transitionTo(self.S1_run)
                        
                    else:
                        # It sets the reference value for the next iteration
                        self.ref.close()
                        Shares.Omega_ref = 30                                          
                        Shares.Position = 0
                        Shares.Duty_cycle = 0
                        Shares.Omega_measured = 0
                        self.ClosedLoop.run()
                        self.transitionTo(self.S0_init)
                     
                    
                    
                
                    
                    
               
## Task object
task1 = main()   # Will run a task for the the encoder

while True:    #Will change to   "while True:" once we're on hardware
   task1.run()     
                    
                
                
                    
                                                       
        
        
        
        
        
        
        
        