"""
@file Shares.py

@brief A container for all the inter-task variables

Created on Mon Oct 19 11:55:17 2020

@author: horac
"""

# Provides with the encoder position
Position = 0

## The command character used in the Encoder and User_Interface class to know the delta value
Delta = 0             # Units of ticks

# It sets a variable for the duty cycle at which the motor has to operate
Duty_cycle = 0      # Percentage value

# It set a global variable for the reference velocity in RPM, maximum speed is 18000 rev/min or around 1880 rad/s
Omega_ref = 30    # Units of RPM

# It sets the array of measurements for the measured velocity provided in the ComputerInteraction2.py
Omega_measured = 0    

# It provides with a global variable in whick K_p is set up
K_p = 3.3         # Unitless, variable for velocity in duty_cycle equation

# It provides with a global variable in which K_p is set up for the delta value 
K_p2 = 3.3       # Unitless, variable for delta value in duty_cycle equation

# It provies with a global variable for the voltage provided
V_DC = 3.3

# It provides with the variable to start running the data on the main file
Condition = 0        # 1=True, and it runs the data provided in the main file

# It sets a global variable with the uart.any() command
Check = None

# It establishes a global variable for the performance of the motor
J = 0

# It sets a global variable with the reference position
Position_ref = 0

# It provides with a global start time
start_time = 0

