"""
@file EncoderDriver.py

This file will provide with a code in which some of the properties of the Encoder
will be define as well as interacting with a User 

Created on Tue Oct 13 13:24:42 2020

@author: horac
"""

import utime  
import pyb 
import Shares
from pyb import UART


class TaskEncoder:
    '''
    @brief This will encapsulate the operation of the timer to read from an encoder
    @details This class will generate cetain properties of an Encoder connected to a
                 Nucleo-L476RG
    '''
    
    
    def __init__(self):     
        '''
        @brief creates an encoder object
        
        '''
        # It initialize the baudrate
        self.myuart = UART(2)
        
        # Defining the tim variable for the timer used, we used Timer(3)
        self.timer = pyb.Timer(4)
    
        # Provides with the prescalar and period for the input variable
        self.timer.init(prescaler=0, period=0XFFFF)
        
        # It sets a variable with the pulses-per-revolution for the given motor
        self.PPR = 16000   # Accounting for gear ration(4:1), and converting from CPR to PPR
        
        # Sets a vaariable with the convertion factor for ticks to degree
        self.ticks_to_degrees = 16000/360  # Units of ticks/degree
        
        # It defines the corresponding properties for channel 1
        self.Ch1 = self.timer.channel (1, pin=pyb.Pin.cpu.B6, mode=pyb.Timer.ENC_AB)
        
        # It defines the corresponding properties for channel 2
        self.Ch2 = self.timer.channel (2, pin=pyb.Pin.cpu.B7, mode=pyb.Timer.ENC_AB)
    
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The time since 2000-01-01
        
        # It sets the time for the calculation of omega_measured
        self.Old_Time = utime.ticks_us()
        
        # It provides with the old reading of the encoder position
        self.En_old = self.timer.counter()
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(25e3)                 
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def update (self):  
        '''
        @brief It creates an object which contains the recorded position of the Encoder
               which helps know if the old position of the encoder has changed
        '''
        # it updates the value of the Encoder
        self.En_Update = self.timer.counter()                 # Provides with units of ticks
        self.Delta_Check = self.En_Update - self.En_old       # It updates the value of delta 
        
    def get_position(self):
        '''
        @brief It creates an object with the most recent update position of the Encoder and prints it
        
        '''
        # it defines the updated value for the input provided by the Nucleo
        self.En_Update = self.timer.counter()                  # Provides with units of ticks
        self.Update_Time = utime.ticks_us()                      # Provides with time in microsec
        #print (' The Updated value for the counter is {:0.2f}'.format(self.En_Position))
        
    def set_position(self, OldReading, OldTime):
        '''
        @brief It create an object which contains the old position and time of the Encoder
        '''
        
        self.En_old = OldReading                                # It provides with the old reading from the encoder to the individual states
        self.Old_Time = OldTime                                 # It sets the time when the old reading was calculated
        
    def get_delta(self):
        '''
        @brief It creates an object the provides with the difference in recorded positions
        '''
        #self.Delta_Check = self.En_Update - self.En_old   # It provides with the raw delta value to be check whether is good or bad
        #print(self.Delta_Check)
        if self.Delta_Check == 0:                             
            Shares.Delta = 0                                                # It has units of ticks/pulses
        elif self.Delta_Check > (0xFFFF/2):                                 # underflow case
            Shares.Delta = -((0xFFFF - self.En_old) + self.En_Update)
        elif self.Delta_Check < -(0xFFFF/2):                                # overflow case
            Shares.Delta = ((0xFFFF - self.En_old) + self.En_Update)
        else:                                                               # Normal condition
            Shares.Delta = self.Delta_Check
            
        # It provides witht the encoder position in degrees
        Shares.Position += ((Shares.Delta)/self.ticks_to_degrees)           # Provides with units of degree
        #print(Shares.Position)
        
    def Vmea(self):
        '''
        @brief It calculated the velocity of the Motor
        '''
        self.DeltaTime = ((self.Update_Time) - (self.Old_Time))*1e-6               # It provides delta_time in sec
        #print(self.DeltaTime)                                                     # For coding debug
        #print(Shares.Delta)                                                       # For coding debug
        Shares.Omega_measured = ((Shares.Delta)/(self.DeltaTime))*(60/self.PPR)    # Units of RPM
        #print(Shares.Omega_measured)                                              # For coding debug
        
    def run(self):
        '''
        @brief   It runs the iteration for the Encoder input
        @details It will continuosly provided with the input provided by the 
                 encoder motor
        '''
            # self.curr_time = utime.ticks_us()
            # if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):            
            #     self.next_time = utime.ticks_add(self.start_time, self.interval)
        self.update()                                                          # It updates the position of the encoder for state 1
        if self.En_Update != self.En_old:                                      # Check if the value of the Encoder has changed
            self.get_position()                                                # It prints the recent position of the Encoder for State 1
            self.get_delta()
            self.Vmea()
            self.set_position(self.En_Update, self.Update_Time)                # Setting old reading of Encoder for state 1 based on the Encoder reading of state 2
        
           
           
                
                
        
        
          
        
        
        
