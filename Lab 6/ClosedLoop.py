"""
@file ClosedLoop.py

@details It is a file for the Interaction of an User Interface which is controled through Python


Created on Tue Oct 20 12:49:45 2020

@author: horac
"""
import utime
import pyb
from pyb import UART
import Shares

class DataCollection:
    '''
    @brief It creates a class that will provide information about the Encoder
    @details It will create a class that will compute information about the Encoder
             position as well as communicating with the Computer Interaction code
             in order to provide with data for the User
    '''
    
    def __init__(self):
        '''
        @brief It creates an object for the DataCollection class
        '''
        
        # It initialize the baudrate
        self.myuart = UART(2)
        
         # Defining the tim variable for the timer used
        self.tim4 = pyb.Timer(4)
        self.tim4.init(prescaler=0, period=0XFFFF)
        
        # It defines the corresponding properties for channel 1 that text input
        self.Ch1 = self.tim4.channel(1, pin=pyb.Pin.cpu.B6, mode=pyb.Timer.ENC_AB)
        
        # It defines the corresponding properties for channel 2 that recieves text
        self.Ch2 = self.tim4.channel(2, pin=pyb.Pin.cpu.B7, mode=pyb.Timer.ENC_AB)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        # It sets the runs for the period
        self.runs_old = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()    
        
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(0.2e3)     # real time for the code, 1e6=1-second
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    
    def InputCheck(self):
        '''
        @brief It will check the input of the variable provided in order to execute the appropiate task
        '''
        self.check = self.myuart.any()
        self.raw_value = self.myuart.readchar()
        if self.check != 0:
            self.start_time = utime.ticks_us()
            self.modified_value = self.raw_value - 48    # This equation sets a referent value of 0 since in ASCII the zero number corresponds to a 48 decimal value
            if self.modified_value == 0:
                Shares.K_p = 0
                self.modified_value = 0
            elif 0 < self.modified_value <= 9:
                Shares.K_p = 0.1*self.modified_value + 0         # It provides with increments of 0.1 to a nominal value of 2 according to the input given
                self.modified_value = 0
            else:                                                # any other key pressed will provide with a zero value for K_p
                Shares.K_p = 0
        #print(Shares.K_p) 
        #print(self.check)
        
    def Percent(self):
        '''
        @brief It calculates the value of L which is the duty cycle for the motor
        '''
        self.Omega_diff = (Shares.Omega_ref - Shares.Omega_measured)
        self.Duty_cycle = (Shares.K_p/Shares.V_DC)*(self.Omega_diff)
        #print(Shares.Omega_ref - Shares.Omega_measured)            # For code debugging
        #print('Before ' + str(Shares.Duty_cycle))                  # For code debugging
        if (self.Omega_diff) != 0:                                  # Checks if the measured velocity is the same as the velocity desired
            if self.Duty_cycle >= 0:                                # Checks if the cycle is positive and loops for Shares.Duty_cycles less than 100 %
                if (self.Duty_cycle <= 100):
                    Shares.Duty_cycle = self.Duty_cycle
                    
            elif self.Duty_cycle < 0:                               # Checks if the cycle is negative and set a range of values
                if (self.Duty_cycle >= -100):
                    Shares.Duty_cycle = self.Duty_cycle
                    
        elif self.Omega_diff == 0:                                  # when the desired velocity is the same as the measured velocity, it does not provide any more duty_cycle
            Shares.Duty_cycle = 0
        #print('Duty after: {:}'.format(Shares.Duty_cycle))         # Print duty and check its value to see if it makes sense, should be between 0-100%

        
    def run(self):
        '''
        @brief It creates an object through which the DataCollection class will run
        '''
        
        self.InputCheck()
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            self.Percent()
            self.Time = ((utime.ticks_diff(self.curr_time,self.start_time))*1e-6)    # It adds the time in seconds to the time array     
            self.myuart.write('{:0.2f},{:0.2f},{:0.2f}\r\n'.format((Shares.Omega_measured), self.Time, Shares.Position))
            #print('S:{:0.2f}, T:{:0.2f}, P:{:0.2f} R:{:}'.format(Shares.Omega_measured, self.Time, Shares.Position, self.runs))
            self.runs += 1        
            #print(self.Time)
            self.next_time = utime.ticks_add(self.next_time, self.interval)    # Specifying the next time the task will run
                          
               
                           
                       
               
               

                


        
                
                
