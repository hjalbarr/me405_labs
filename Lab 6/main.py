"""
@file  main.py
@details A container for the tasks that need to be run 

@author: horac
"""
import Shares
from EncoderDriver import TaskEncoder
from ClosedLoop import DataCollection
from Motor import MotorDriver
import utime

# Variable for the EncoderDriver code
EncoderDriver = TaskEncoder()  

# Variable for the ClosedLoop code
ClosedLoop = DataCollection() 

# Variable for the Motor code
Motor = MotorDriver() 

## The timestamp for the first iteration
start_time = utime.ticks_us()    
        
## The interval of time, in microseconds, between runs of the task
interval = int(20e3)     # real time for the code, 1e6=1-second
        
## The "timestamp" for when the task should run next
next_time = utime.ticks_add(start_time, interval)

# Code to run all the tasks
while True:  
    curr_time = utime.ticks_us()    #updating the current timestamp
    if utime.ticks_diff(curr_time, next_time) >= 0:
        next_time = utime.ticks_add(next_time, interval)
        ClosedLoop.run()                                          # To collect the data from the encoder
        Motor.enable()                                            # Enables the motor to work
        Motor.set_duty(Shares.Duty_cycle)                         # To set percent of duty cycle on motor
        EncoderDriver.run()                                       # To get the omega reference from encoder 