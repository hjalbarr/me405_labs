"""
@file Shares.py

@brief A container for all the inter-task variables

Created on Mon Oct 19 11:55:17 2020

@author: horac
"""

# Provides with the encoder position
Position = 0

## The command character used in the Encoder and User_Interface class to know the delta value
Delta = 0             # Units of ticks

# It sets a variable for the duty cycle at which the motor has to operate
Duty_cycle = 0       # Percentage value

# It set a global variable for the reference velocity in RPM, maximum speed is 18000 rev/min or around 1880 rad/s
Omega_ref = 1000    # Units of RPM

# It sets the array of measurements for the measured velocity provided in the ComputerInteraction2.py
Omega_measured = 0    

# It provides with a global variable in whick K_p is set up
K_p = 0         # Unitless, check table below for ranges of K_p 

# It provides with a global variable for the voltage provided
V_DC = 3.3



# K_p      Velocity
# 0-1      very slow response
# 1-2      500-1000
# 2-3      1000-1300
# 3-3.3    1300-1600
# >3.3     No response